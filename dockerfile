FROM node:17-alpine
WORKDIR /root
COPY /frontend .
RUN npm install --production
CMD ["npm", "start"]