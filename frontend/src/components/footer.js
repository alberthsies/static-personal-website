function Footer() {
    return (
        <div className="container flex-row footer">
            <p id="copyright">© 2021-22 Yuan-Shen Hsieh</p>
            <div className="flex-row footer-icon">
                <a href="https://gitlab.com/ys-hsieh" target="_blank" rel="noreferrer">
                    <i class="fab fa-gitlab icon-btn"></i>
                </a>
                <a href="https://github.com/alberthsies" target="_blank" rel="noreferrer">
                    <i class="fab fa-github icon-btn"></i>
                </a>
                <a href="https://www.linkedin.com/in/ys-hsieh/" target="_blank" rel="noreferrer">
                    <i class="fab fa-linkedin icon-btn"></i>
                </a>
                <a href="mailto:contact@yshsieh.link" target="_blank" rel="noreferrer">
                    <i class="far fa-envelope icon-btn"></i>
                </a>
            </div>
        </div>
    );
}

export default Footer;