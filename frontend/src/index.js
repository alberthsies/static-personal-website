import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './assets/global.css';
import './index.css';
import Header from './components/header';
import Footer from './components/footer';
import Home from './models/Home';
import Photo from './models/Photo';
import Blog from './models/Blog';
import Project from './models/Project';
import About from './models/About';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <div className="flex-col context">
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="photo" element={<Photo />} />
          <Route path="blog" element={<Blog />} />
          <Route path="project" element={<Project />} />
          <Route path="about" element={<About />} />
        </Routes>
        <Footer />
      </div>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
